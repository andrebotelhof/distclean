
public class Produto {
	private int codProduto;
	private String nome;
	private String categoria;
	private double preco;
	
	public Produto(int codProduto, String nome, String categoria, double preco) {
		this.codProduto=codProduto;
		this.nome=nome;
		this.categoria=categoria;
		this.preco=preco;
	}

	public int getCodProduto() {
		return codProduto;
	}

	public void setCodProduto(int codProduto) {
		this.codProduto = codProduto;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}
	
}
