import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ClienteBODAO {
	
	public static ArrayList<Cliente> listClientes() throws SQLException {
		ArrayList<Cliente> clientes = new ArrayList<>();
		Connection conexao = DriverManager.getConnection(Conexao.endereco, Conexao.user, Conexao.password);
		String sql = "SELECT CODCLIENTE, NOME, ENDERECO , TELEFONE, STATUS FROM CLIENTE ORDER BY NOME";
		PreparedStatement stmt = conexao.prepareStatement(sql);
		//stmt.setString(1, cliente);
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			Integer codCliente = rs.getInt("CODCLIENTE");
			String nome = rs.getString("NOME");
			String endereco = rs.getString("ENDERECO");
			String telefone = rs.getString("TELEFONE");
			String status = rs.getString("STATUS");
		 	clientes.add(new Cliente(codCliente, nome, endereco, telefone, status));
		}
		rs.close();
		stmt.close();
		conexao.close();
		return clientes;
	}
	
	public static ArrayList<Cliente> buscaCliente(String cliente) throws SQLException {
		ArrayList<Cliente> clientes = new ArrayList<>();
		Connection conexao = DriverManager.getConnection(Conexao.endereco, Conexao.user, Conexao.password);
		String sql = "SELECT CODCLIENTE, NOME, ENDERECO , TELEFONE, STATUS FROM CLIENTE WHERE NOME LIKE '%" + cliente + "%'";
				PreparedStatement stmt = conexao.prepareStatement(sql);
				//stmt.setString(1, cliente);
				ResultSet rs = stmt.executeQuery();
				while (rs.next()) {
					Integer codCliente = rs.getInt("CODCLIENTE");
					String nome = rs.getString("NOME");
					String endereco = rs.getString("ENDERECO");
					String telefone = rs.getString("TELEFONE");
					String status = rs.getString("STATUS");
				 	clientes.add(new Cliente(codCliente, nome, endereco, telefone, status));
				}
		rs.close();
		stmt.close();
		conexao.close();
		return clientes;
	}
}
